#!/bin/bash
set -xue

# set booleans
export REGISTER_LEAVE_RUNNER=false
export REGISTER_RUN_UNTAGGED=false
export REGISTER_LOCKED=true
export DOCKER_PRIVILEGED=true

gitlab-runner register \
    --non-interactive \
    --registration-token {{item.REGISTRATION_TOKEN}} \
    --url {{item.CI_SERVER_URL|default(CI_SERVER_URL)}} \
    --tag-list {{item.RUNNER_TAG_LIST|default(RUNNER_TAG_LIST)}} \
    --config {{CONFIG_FILE|default('/etc/gitlab-runner/config.toml')}} \
    --executor {{RUNNER_EXECUTOR|default('docker')}} \
    --limit {{RUNNER_LIMIT|default(5)}} \
    --request-concurrency {{RUNNER_LIMIT|default(5)}} \
    --maximum-timeout {{REGISTER_MAXIMUM_TIMEOUT|default(36000)}} \
    --output-limit {{RUNNER_OUTPUT_LIMIT|default(8192)}} \
    --docker-image {{DOCKER_IMAGE|default('alpine:latest')}} \
{% if RUNNER_EXECUTOR == 'docker+machine' %}
    --machine-machine-driver {{MACHINE_DRIVER}} \
    --machine-machine-name {{MACHINE_NAME|default('%s')}} \
    --machine-max-builds {{MACHINE_MAX_BUILDS|default(1)}} \
    --machine-machine-options "engine-install-url={{item.MACHINE_DOCKER_INSTALL_URL|default(MACHINE_DOCKER_INSTALL_URL)}}" \
    {% if MACHINE_DOCKER_STORAGE_DRIVER is defined %}
    --machine-machine-options "engine-storage-driver={{item.MACHINE_DOCKER_STORAGE_DRIVER|default(MACHINE_DOCKER_STORAGE_DRIVER)}}" \
    {% else %}
    --machine-machine-options "engine-storage-driver={{item.MACHINE_DOCKER_STORAGE_DRIVER|default('devicemapper')}}" \
    {% endif %}
{% if MACHINE_DRIVER == 'openstack' %}
    --machine-machine-options "openstack-auth-url={{OS_AUTH_URL}}" \
    {% if OS_APPLICATION_CREDENTIAL_ID is defined %}
    --machine-machine-options "openstack-application-credential-id={{OS_APPLICATION_CREDENTIAL_ID}}" \
    --machine-machine-options "openstack-application-credential-secret={{OS_APPLICATION_CREDENTIAL_SECRET}}" \
    {% else %}
    --machine-machine-options "openstack-username={{OS_USERNAME}}" \
    --machine-machine-options "openstack-password={{OS_PASSWORD}}" \
    {% endif %}
    --machine-machine-options "openstack-domain-name={{OS_DOMAIN_NAME}}" \
    --machine-machine-options "openstack-region={{OS_REGION_NAME}}" \
    --machine-machine-options "openstack-tenant-name={{OS_TENANT_NAME}}" \
    --machine-machine-options "openstack-net-name={{OS_NETWORK_NAME}}" \
    --machine-machine-options "openstack-sec-groups={{OS_SECURITY_GROUPS}}" \
    --machine-machine-options "openstack-user-data-file={{OS_USER_DATA_FILE}}" \
    --machine-machine-options "openstack-ssh-user={{OS_SSH_USER}}" \
    --machine-machine-options "openstack-flavor-name={{item.OS_FLAVOR_NAME|default(OS_FLAVOR_NAME)}}" \
    --machine-machine-options "openstack-image-id={{item.OS_IMAGE_ID|default(OS_IMAGE_ID)}}" \
{% elif MACHINE_DRIVER == 'rackspace' %}
    --machine-machine-options "rackspace-username={{OS_USERNAME}}" \
    --machine-machine-options "rackspace-api-key={{OS_API_KEY}}" \
    --machine-machine-options "rackspace-region={{OS_REGION_NAME}}" \
    --machine-machine-options "rackspace-ssh-user={{OS_SSH_USER}}" \
    --machine-machine-options "rackspace-flavor-id={{item.RS_FLAVOR_ID|default(RS_FLAVOR_ID)}}" \
    --machine-machine-options "rackspace-image-id={{item.RS_IMAGE_ID|default(RS_IMAGE_ID)}}" \
{% endif %}
{% endif %}
    "$@"

# for new tag samba-ci-private, use ubuntu1804 and 8G RAM
# for legacy, use ubuntu1804 and 16G RAM
