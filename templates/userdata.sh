#!/bin/bash

apt-get update -y && apt-get install -y rng-tools && /etc/init.d/rng-tools start

echo 0 > /proc/sys/kernel/yama/ptrace_scope
echo 0 > /proc/sys/kernel/perf_event_paranoid
mount / -o remount,rw,nobarrier
