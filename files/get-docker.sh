#!/bin/bash
set -xue

# docker-machine will run this script to install docker
# we can inject our own cmds here to bootstrap the docker host server
# and specify docker version to install
sudo apt-get update
sudo apt-get install -y rng-tools
sudo /etc/init.d/rng-tools start

echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
echo 0 | sudo tee /proc/sys/kernel/perf_event_paranoid
sudo mount / -o remount,rw,nobarrier

# specify an old version for devicemapper required by samba tests
export VERSION=18.06
curl -sSL https://get.docker.com | sh -
